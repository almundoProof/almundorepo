var port = Number(process.env.PORT || 8000);
var express = require('express'); 
var mongoose  = require('mongoose');
var bodyParser = require('body-parser');

// Conexión con la base de datos
mongoose.connect('mongodb://localhost:27017/AlMundoDB');

var hotel = mongoose.model('Hotel', {
  name: String,
  status: String,
  stars: Number,
  price: Number,
  city: String,
  imgUrl: Array,
  features: String,
  recommended: Boolean,
  discount: Number,
  payDestiny: Boolean,
  payDues: Boolean,
  created_at: Date
});

var app = express();
app.use(bodyParser.json({limit: '8mb'}));

// Rutas de nuestro API
app.get('/api/hotels', function(req, res) {  
  hotel.find(function(err, datas) {
    if(err) {
      res.send(err);
    }
    res.json(datas);
  });
});

// POST que crea un HOTEL y devuelve todos tras la creación
app.post('/api/hotels', function(req, res) {
  hotel.create({
    name: req.body.name,
    status: req.body.status,
    stars: req.body.stars,
    price: req.body.price,
    city: req.body.city,
    imgUrl: req.body.imgUrl,
    features: req.body.features,
    recommended: req.body.recommended,
    payDestiny: req.body.payDestiny,
    payDues: req.body.payDues,
    discount: req.body.discount,
    created_at: req.body.created_at 
  }, function(err, htl){
  
    if(err) {res.send(err);}

    hotel.find(function(err, datas) {
      if(err){res.send(err);}
      res.json(datas);
    });
  });
});

// DELETE un HOTEL específico y devuelve todos tras borrarlo.
app.delete('/api/hotels/:hotel', function(req, res) {  
  hotel.findByIdAndRemove(req.params.hotel, function (err, htl) {
    hotel.find(function(err, datas) {
      if(err){res.send(err);}
      res.json(datas);
    });
  });
});

app.use(express.static(__dirname + '/app'));
var server = app.listen(port, function() { console.log('Listening on port %d', server.address().port); });