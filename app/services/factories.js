'use strict';

angular.module('myApp.factory', ['ngRoute'])

.factory('hotelFactory', function($http){
  return {
    getHotels: function() {
      return $http.get('/api/hotels', {});
    },
    createHotel: function(param) {
    	return $http.post('/api/hotels', param)
    },
    deleteHotel: function(id) {
    	return $http.delete('/api/hotels/' + id)
    }
  };
});