'use strict';
// Declare app level module which depends on views, and components
angular.module('myApp', ['ngRoute', 'myApp.hotel', 'myApp.admin', 'myApp.factory', 'myApp.version', 'ngFileUpload', 'myApp.routes'])

.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');
  $routeProvider.otherwise({redirectTo: '/list_hotels'});
}]);
