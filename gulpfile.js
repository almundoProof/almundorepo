var gulp = require('gulp'),
  jsmin = require('gulp-jsmin'),
  rename = require('gulp-rename'),
  minifyCSS = require('gulp-minify-css'),
  concatCss = require('gulp-concat-css'),
  htmlmin = require('gulp-htmlmin'),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat');

gulp.task('js', function () {
  gulp.src('app/hotel/hotel.js')
    .pipe(jsmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('app/hotel/min'));
  gulp.src('app/admin/admin.js')
    .pipe(jsmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('app/admin/min'));
  gulp.src(['app/admin/min/admin.min.js', 'app/hotel/min/hotel.min.js'])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('app'));
});

gulp.task('css', function () {
  gulp.src('app/app.css')
    .pipe(concatCss("app.min.css"))
    .pipe(minifyCSS({keepBreaks:false}))
    .pipe(gulp.dest('app/'));
});

gulp.task('html', function() {
  gulp.src('app/hotel/hotel.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app/hotel/min'));
  gulp.src('app/admin/admin.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app/admin/min'));
});